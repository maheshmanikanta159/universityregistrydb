package com.example.UniversityRegistryDB.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.UniversityRegistryDB.model.Student;
import com.example.UniversityRegistryDB.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController
{
	@Autowired
	private StudentService ss;
	
	@GetMapping("/showstudents")
	public ResponseEntity<List<Student>> showStudents()
	{
		return ResponseEntity.ok().body(ss.listStudents());
	}
	
	@GetMapping("/showstudentbyid/{id}")
	public ResponseEntity<Student> showStudents(@PathVariable Integer id)
	{
		return ResponseEntity.ok().body(ss.getStudentById(id));
	}
	
	@GetMapping("/showstudentbyname/{name}")
	public ResponseEntity<Student> showStudents(@PathVariable String name)
	{
		return ResponseEntity.ok().body(ss.getStudentByName(name));
	}
	
	@PostMapping("/addstudent")
	public ResponseEntity<Student> addStudent(@RequestBody Student s)
	{
		return ResponseEntity.ok().body(ss.add(s));
	}
	
	@PutMapping("/updatestudent/{id}")
	public ResponseEntity<Student> updateStudent(@RequestBody Student s,@PathVariable Integer id)
	{
		return ResponseEntity.ok().body(ss.update(id, s));
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/deletestudent/{id}")
	public ResponseEntity<Student> deleteStudent(@PathVariable Integer id)
	{
		return ResponseEntity.ok().body(ss.deleteStudents(id));
	}
}
