package com.example.UniversityRegistryDB.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.UniversityRegistryDB.model.Student;
import com.example.UniversityRegistryDB.repository.StudentRepository;

@Service
public class StudentService 
{
	@Autowired
	StudentRepository sturepo;
	
	public Student add(Student s)
	{
		sturepo.save(s);
		return s;
	}
	 
	 public Student getStudentByName(String studentname)
	 {
		 for(Student s:sturepo.findAll())
		 {
			 if(s.getName()==studentname)
			 {
				 return s;
			 }
		 }
		 return null;
	 }
	 
	 public Student getStudentById(Integer Id)
	 {
		 return sturepo.findById(Id).get();
	 }
	 
	 public List<Student> listStudents()
	 {
		 List<Student> students=new ArrayList<Student>();
		 for(Student s:sturepo.findAll())
		 {
			 students.add(s);
		 }
		 return students;
	 }
	 
	 public Student deleteStudents(Integer studentid)
	 {
		 Student s=sturepo.findById(studentid).get();
		 sturepo.delete(s);
		 return s;
	 }
	 
	 public Student update(Integer studentId, Student snew)
	 {
		 Student s=sturepo.findById(studentId).get();
		 s.setAddress(snew.getAddress());
		 s.setCoursename(snew.getCoursename());
		 s.setDOB(snew.getDOB());
		 s.setEmail(snew.getEmail());
		 s.setName(snew.getName());
		 s.setPassword(snew.getPassword());
		 s.setPhone(snew.getPhone());
		 s.setUsername(snew.getUsername());
		 sturepo.save(s);
		 return s;
	 }
}
