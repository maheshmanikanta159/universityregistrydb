package com.example.UniversityRegistryDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityRegistryDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversityRegistryDbApplication.class, args);
	}

}
