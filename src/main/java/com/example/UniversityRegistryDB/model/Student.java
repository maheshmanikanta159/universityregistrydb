package com.example.UniversityRegistryDB.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="StudentRegistration")
public class Student 
{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="id")
	private int studentId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="address")
	private String address;
	
	@Column(name="phone")
	private long phone;
	
	@Column(name="DOB")
	private String DOB;
	
	@Column(name="email")
	private String email;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@Column(name="courseName")
	private String coursename;
	
	

	

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", name=" + name + ", address=" + address + ", phone=" + phone
				+ ", DOB=" + DOB + ", email=" + email + ", username=" + username + ", password=" + password
				+ ", coursename=" + coursename + "]";
	}

	public Student(String name, String address, long phone, String dOB, String email, String username,
			String password, String coursename) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		DOB = dOB;
		this.email = email;
		this.username = username;
		this.password = password;
		this.coursename = coursename;
	}

	public Student() {
		super();
	}

	public Student(int studentId, String name, String address, long phone, String dOB, String email, String username,
			String password, String coursename) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.address = address;
		this.phone = phone;
		DOB = dOB;
		this.email = email;
		this.username = username;
		this.password = password;
		this.coursename = coursename;
	}

	
	
	
	
}
