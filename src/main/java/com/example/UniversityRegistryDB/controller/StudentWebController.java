package com.example.UniversityRegistryDB.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.UniversityRegistryDB.model.Student;
import com.example.UniversityRegistryDB.service.StudentService;

@Controller
public class StudentWebController
{
	@Autowired
	private StudentService ss;
	
	@RequestMapping("/register")
	public ModelAndView register()
	{
		ModelAndView mv=new ModelAndView("register");
		mv.addObject("register");
		return mv;
	}
	
	
	@RequestMapping("/registerUpdate")
	public ModelAndView registerUpdate()
	{
		ModelAndView mv=new ModelAndView("registerUpdate");
		mv.addObject("registerUpdate");
		return mv;
	}
	
	@RequestMapping("/registerDelete")
	public ModelAndView registerDelete()
	{
		ModelAndView mv=new ModelAndView("registerDelete");
		mv.addObject("registerDelete");
		return mv;
	}
	
	@RequestMapping("/registeredstudents")
	public ModelAndView registeredstudents()
	{
		ModelAndView mv=new ModelAndView("registeredstudents");
		List<Student> stulist=ss.listStudents();
		mv.addObject("student", stulist);
		return mv;
	}
}
