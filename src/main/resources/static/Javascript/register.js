$(document).ready(function ()
{
	$("#studentregister").submit(function(event){
		event.preventDefault();
		registerData();
	});
	$("#Students").click(function(){
		displayData();
	});
	
	$("#updateStudent").submit(function(event){
		event.preventDefault();
		updateData();
	});
	
	$("#deleteStudent").submit(function(event){
		event.preventDefault();
		var id=$("Id").val();
		deleteSData(id);
	});
});





function registerData()
{
	var name=$("#name").val();
	var dob=$("#DOB").val();
	var address=$("#address").val();
	var phone=$("#phone").val();
	var email=$("#email").val();
	var username=$("#username").val();
	var password=$("#password").val();
	var course=$("#coursename").val();
	var jsonformdata={};
	jsonformdata["name"]=name;
	jsonformdata["dob"]=dob;
	jsonformdata["address"]=address;
	jsonformdata["phone"]=phone;
	jsonformdata["email"]=email;
	jsonformdata["username"]=username;
	jsonformdata["password"]=password;
	jsonformdata["coursename"]=course;
	$.ajax({
		type:"POST",
		url:"http://localhost:8080/student/addstudent",
		data:JSON.stringify(jsonformdata),
		contentType:"application/json",
		success:function(Sdata)
		{
		 console.log(Sdata);
		  let html="<table>";
			html+="<tr><td>Id</td><td>Name</td><td>Email</td><td>Username</td><td>Address</td><td>Phone</td><td>DOB</td>+<td>Course</td></tr>"
			html+="<tr>";
			html+="<td>"+Sdata.studentId+"</td>";
			html+="<td>"+Sdata.name+"</td>";
			html+="<td>"+Sdata.name+"</td>";
			html+="<td>"+Sdata.username+"</td>";
			html+="<td>"+Sdata.address+"</td>";
			html+="<td>"+Sdata.phone+"</td>";
			html+="<td>"+Sdata.dob+"</td>";
			html+="<td>"+Sdata.coursename+"</td>";
			html+="</tr></table>";
		  $("#studentData").html(html);
		  console.log("Success:"+html);
		},
		error:function(err)
		{
			console.log(err.responseText);
			alert(err.responseText);
		  $("#studentData").html("<h2>"+e.responseText+"</h2>");
		}
	});
}


function displayData()
{
		$.getJSON("http://localhost:8080/student/showstudents",function(data)
		{
			let html="<table>";
			html+="<tr><td>Id</td><td>Name</td><td>Email</td><td>Username</td><td>Address</td><td>Phone</td><td>DOB</td>+<td>Course</td></tr>"
			for(var i=0;i<data.length;i++)
			{
				html+="<tr>";
				html+="<td>"+data[i].studentId+"</td>";
				html+="<td>"+data[i].name+"</td>";
				html+="<td>"+data[i].name+"</td>";
				html+="<td>"+data[i].username+"</td>";
				html+="<td>"+data[i].address+"</td>";
				html+="<td>"+data[i].phone+"</td>";
				html+="<td>"+data[i].dob+"</td>";
				html+="<td>"+data[i].coursename+"</td>";
				html+="</tr>"
			}
			html+="</table>";
			$("#studentData").append(html);
		});
}


function updateData()
{
	var id=$("#Id").val();
	var name=$("#name").val();
	var dob=$("#DOB").val();
	var address=$("#address").val();
	var phone=$("#phone").val();
	var email=$("#email").val();
	var username=$("#username").val();
	var password=$("#password").val();
	var course=$("#coursename").val();
	var jsonformdata={};
	jsonformdata["name"]=name;
	jsonformdata["dob"]=dob;
	jsonformdata["address"]=address;
	jsonformdata["phone"]=phone;
	jsonformdata["email"]=email;
	jsonformdata["username"]=username;
	jsonformdata["password"]=password;
	jsonformdata["coursename"]=course;
	$.ajax({
		type:"PUT",
		url:"http://localhost:8080/student/updatestudent/"+id,
		data:JSON.stringify(jsonformdata),
		contentType:"application/json",
		success:function(Sdata)
		{
		 console.log(jsonformdata);
		  let html="<table>";
			html+="<tr><td>Id</td><td>Name</td><td>Email</td><td>Username</td><td>Address</td><td>Phone</td><td>DOB</td>+<td>Course</td></tr>"
			html+="<tr>";
			html+="<td>"+Sdata.studentId+"</td>";
			html+="<td>"+Sdata.name+"</td>";
			html+="<td>"+Sdata.name+"</td>";
			html+="<td>"+Sdata.username+"</td>";
			html+="<td>"+Sdata.address+"</td>";
			html+="<td>"+Sdata.phone+"</td>";
			html+="<td>"+Sdata.dob+"</td>";
			html+="<td>"+Sdata.coursename+"</td>";
			html+="</tr></table>";
		  $("#studentData").html(html);
		  console.log("Success:"+html);
		},
		error:function(err)
		{
			console.log(err.responseText);
			alert(err.responseText);
		  $("#studentData").html("<h2>"+e.responseText+"</h2>");
		}
	});
	
}

function deleteSData(id)
	{
		$.ajax({
		type:"DELETE",
		url:"http://localhost:8080/student/deletestudent/"+id,
		success:function(data)
		{
		  let html="<table>";
			html+="<tr><td>Id</td><td>Name</td><td>Email</td><td>Username</td><td>Address</td><td>Phone</td><td>DOB</td>+<td>Course</td></tr>"
			html+="<tr>";
			html+="<td>"+data.studentId+"</td>";
			html+="<td>"+data.name+"</td>";
			html+="<td>"+data.name+"</td>";
			html+="<td>"+data.username+"</td>";
			html+="<td>"+data.address+"</td>";
			html+="<td>"+data.phone+"</td>";
			html+="<td>"+data.dob+"</td>";
			html+="<td>"+data.coursename+"</td>";
			html+="</tr></table>";
		  $("#studentData").html(html);
		  console.log("Success:"+html);
		},
		error:function(err)
		{
			console.log(err.responseText);
			alert(err.responseText);
		  $("#studentData").html("<h2>"+err.responseText+"</h2>");
		}
		});
	}