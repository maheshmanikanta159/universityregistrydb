<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="webjars/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="Javascript/register.js"></script>
</head>
<body>
	
	<form:form id="updateStudent" method="post">
		ID:<input type="number" name="Id" id="Id"><br>
		Name:<input type="text" name="name" id="name"><br>
		Address:<input type="text" name="address" id="address"><br>
		Phone:<input type="text" name="phone" id="phone"><br>
		Date of Birth:<input type="date" name="DOB" id="DOB"><br>
		Email:<input type="email" name="email" id="email"><br>
		Username:<input type="text" name="username" id="username"><br>
		Password:<input type="password" name="password" id="password"><br>
		Course name:<input type="text" name="coursename" id="coursename"><br>
		<input type="submit" value="Update" id ="submit">
		<br><br>
	</form:form>
	<div id="studentData"></div>
</body>
</html>