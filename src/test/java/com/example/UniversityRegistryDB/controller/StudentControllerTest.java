package com.example.UniversityRegistryDB.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.UniversityRegistryDB.model.Student;
import com.example.UniversityRegistryDB.service.StudentService;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes= {StudentControllerTest.class})
public class StudentControllerTest 
{
	@Mock
	StudentService ss;
	
	@InjectMocks
	StudentController sc;
	
	@Test
	@Order(4)
	@DisplayName("Show all Students")
	public void test_ShowStudents()
	{
		List<Student> stulist=new ArrayList<Student>();
		stulist.add(new Student("Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB"));
		stulist.add(new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA"));
		
		when(ss.listStudents()).thenReturn(stulist);    //Mocking STatement
		
		assertEquals(2,sc.showStudents().getBody().size());
	}
	
	@Test
	@Order(5)
	@DisplayName("Show Students by id")
	public void test_ShowStudentById()
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		//Student stu2=new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
		//		"12345", "BBA");
		int id=1;
		when(ss.getStudentById(id)).thenReturn(stu);    //Mocking STatement
		
		assertEquals(stu.getStudentId(),sc.showStudents(id).getBody().getStudentId());
		
	}
	
	@Test
	@Order(6)
	@DisplayName("Show Students by name")
	public void test_ShowStudentByName()
	{
		Student stu2=new Student(2,"Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
						"12345", "BBA");
		String name="Sony";
		when(ss.getStudentByName(name)).thenReturn(stu2);
		assertEquals(stu2.getName(),sc.showStudents(name).getBody().getName());
	}
	
	@Test
	@Order(1)
	@DisplayName("Add Students")
	public void test_addStudent()
	{
		Student stu2=new Student(2,"Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA");
		
		when(ss.add(stu2)).thenReturn(stu2);
		assertEquals(stu2,sc.addStudent(stu2).getBody());
	}
	
	@Test
	@Order(2)
	@DisplayName("Update Students")
	public void test_UpdateStudents()
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		Student stu2=new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA");
		int id=1;
		
		when(ss.update(id,stu2)).thenReturn(stu2);
		assertEquals(stu2.getUsername(),sc.updateStudent(stu2,id).getBody().getUsername());
	}
	
	
	@Test
	@Order(3)
	@DisplayName("Delete Students")
	public void test_deleteStudents()
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		int id=1;
		
		when(ss.deleteStudents(id)).thenReturn(stu);
		assertEquals(stu,sc.deleteStudent(id).getBody());
	}
}
