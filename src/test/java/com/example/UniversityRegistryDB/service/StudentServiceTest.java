package com.example.UniversityRegistryDB.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.UniversityRegistryDB.model.Student;
import com.example.UniversityRegistryDB.repository.StudentRepository;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes= {StudentServiceTest.class})
public class StudentServiceTest 
{
	@Mock
	StudentRepository sturepo;
	
	@InjectMocks
	StudentService stuservice;
	
	@Test
	@Order(6)
	@DisplayName("Get All Students")
	public void test_listStudents()
	{
		List<Student> students=new ArrayList<Student>();
		students.add(new Student("Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB"));
		students.add(new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA"));
		when(sturepo.findAll()).thenReturn(students);   //Mocking Statement
		
		assertEquals(2,stuservice.listStudents().size());
	}
	
	@Test
	@Order(5)
	@DisplayName("Get Student By Id")
	public void test_getStudentById()
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		Optional<Student> stuop=Optional.of(stu);
		Integer id=1;
		
		
		when(sturepo.findById(id)).thenReturn(stuop);  //Mocking Statement
		
		assertEquals(id,stuservice.getStudentById(id).getStudentId());
	}
	
	
	@Test
	@Order(4)
	@DisplayName("Get Student By Name")
	public void test_getStudentByName()
	{
		List<Student> students=new ArrayList<Student>();
		students.add(new Student("Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB"));
		students.add(new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA"));
		
		String name="Sony";
		when(sturepo.findAll()).thenReturn(students);
		
		assertEquals("Sony",stuservice.getStudentByName(name).getName());
	}
	
	@Test
	@Order(1)
	@DisplayName("Add Student")
	public void test_addStudent()
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		when(sturepo.save(stu)).thenReturn(stu);  //Mocking Statement
		
		assertEquals(stu,stuservice.add(stu));
	}
	
	@Test
	@Order(2)
	@DisplayName("Update Student")
	public void test_updateStudent()
	{
		Student stu1=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		Student stu2=new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA");
		Optional<Student> stuop=Optional.of(stu1);
		int id=1;
		when(sturepo.findById(id)).thenReturn(stuop);
		when(sturepo.save(stuop.get())).thenReturn(stuop.get());
		assertEquals(stu2.getUsername(),stuservice.update(id, stu2).getUsername());
	}
	
	 @Test
	 @Order(3)
	 @DisplayName("Delete Student")
	 public void test_deleteStudent()
	 {
		 Student stu1=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
					"1234", "LLB");
		 Optional<Student> stuop=Optional.of(stu1);
		 int id=1;
		 when(sturepo.findById(id)).thenReturn(stuop);
		 
		 //when(sturepo.delete(stuop.get())).thenReturn(stuop.get());  //Mocking Statement
		 stuservice.deleteStudents(id);
		 verify(sturepo,times(1)).delete(stuop.get());           //Mocking Statement
	 }
}