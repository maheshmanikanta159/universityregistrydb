package com.example.UniversityRegistryDB.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.UniversityRegistryDB.model.Student;

public interface StudentRepository extends CrudRepository<Student,Integer>
{
	
}
