package com.example.UniversityRegistryDB.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.UniversityRegistryDB.model.Student;
import com.example.UniversityRegistryDB.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;



@TestMethodOrder(OrderAnnotation.class)
@ComponentScan(basePackages="com.example.UniversityRegistryDB")
@AutoConfigureMockMvc
@ContextConfiguration
@SpringBootTest(classes= {ControllerMockMvcTest.class})
public class ControllerMockMvcTest 
{
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objmap;
	
	@Mock
	StudentService ss;
	
	@InjectMocks
	StudentController sc;
	
	@BeforeEach
	public void setUp()
	{
		mockMvc=MockMvcBuilders.standaloneSetup(sc).build();
	}
	
	@Test
	@Order(1)
	@DisplayName("All Students")
	public void test_getallStudents() throws Exception
	{
		List<Student> stulist=new ArrayList<Student>();
		stulist.add(new Student("Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB"));
		stulist.add(new Student("Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA"));
		
		when(ss.listStudents()).thenReturn(stulist);
		
		this.mockMvc.perform(MockMvcRequestBuilders.get("/student/showstudents"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@Order(2)
	@DisplayName("Students By Id")
	public void test_getStudentsById() throws Exception
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		int id=1;
		
		when(ss.getStudentById(id)).thenReturn(stu);
		
		this.mockMvc.perform(MockMvcRequestBuilders.get("/student/showstudentbyid/{id}",id))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath(".studentId").value(1))
		.andExpect(MockMvcResultMatchers.jsonPath(".username").value("ABC"))
		.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@Order(3)
	@DisplayName("Students By Name")
	public void test_getStudentsByName() throws Exception
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		String name="Nani";
		
		when(ss.getStudentByName(name)).thenReturn(stu);
		
		
		this.mockMvc.perform(MockMvcRequestBuilders.get("/student/showstudentbyname/{name}",name))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath(".name").value("Nani"))
		.andExpect(MockMvcResultMatchers.jsonPath(".username").value("ABC"))
		.andDo(MockMvcResultHandlers.print());
		
		
	}
	
	@Test
	@Order(4)
	@DisplayName("Add Student")
	public void test_AddStudent() throws Exception
	{
		Student stu=new Student(1,"Nani", "MCL", 123456789, "2001-10-10", "abc@gmail.com", "ABC",
				"1234", "LLB");
		
		when(ss.add(stu)).thenReturn(stu);
		
		String jsonBody=objmap.writeValueAsString(stu);
		
		
		this.mockMvc.perform(MockMvcRequestBuilders.post("/student/addstudent")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonBody))
		.andExpect(MockMvcResultMatchers.status().isOk())
//		.andExpect(MockMvcResultMatchers.jsonPath(".studentId").value(1))
//		.andExpect(MockMvcResultMatchers.jsonPath(".name").value("Sony"))
//		.andExpect(MockMvcResultMatchers.jsonPath(".username").value("ABCD"))
		.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@Order(5)
	@DisplayName("Update Student")
	public void test_UpdateStudent() throws Exception
	{
		
		Student stu1=new Student(1,"Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA");
		int id=1;
		
		when(ss.update(id,stu1)).thenReturn(stu1);
		
		ObjectMapper objmap=new ObjectMapper();
		String jsonBody=objmap.writeValueAsString(stu1);
		
		
		this.mockMvc.perform(MockMvcRequestBuilders.put("/student/updatestudent/{id}",id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonBody))
		.andExpect(MockMvcResultMatchers.status().isOk())
//		.andExpect(MockMvcResultMatchers.jsonPath(".studentId").value(1))
//		.andExpect(MockMvcResultMatchers.jsonPath(".name").value("Sony"))
//		.andExpect(MockMvcResultMatchers.jsonPath(".username").value("ABCD"))
		.andDo(MockMvcResultHandlers.print());
	}
	
	
	@Test
	@Order(6)
	@DisplayName("Delete Student")
	public void test_DeleteStudent() throws Exception
	{
		int id=1;
		Student stu1=new Student(1,"Sony", "Durgi", 1234567890, "2000-10-10", "abcd@gmail.com", "ABCD",
				"12345", "BBA");
		when(ss.deleteStudents(id)).thenReturn(stu1);
		
		this.mockMvc.perform(MockMvcRequestBuilders.delete("/student/deletestudent/{id}",id))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print());
	}
	
}


